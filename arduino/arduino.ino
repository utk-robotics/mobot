
/*

   mobot example arduino code


*/



// maximum argument length
#define CMD_ARG_MAX 32
#define CMD_ARG_N 8

#define CMD_INVALID (0)
#define CMD_VALID   (1)

// parsing states (intermediate)
#define CMD_P_ARGS (3)
#define CMD_P_END  (4)


// expects COMMAND:v0,v1,v2;

int cmd_state = CMD_INVALID;
int cmd_argc;

// will be CMD, ARG0, ARG1, ARG....
char cmd_argv[CMD_ARG_N][CMD_ARG_MAX];

int _cmd_a, _cmd_i;

// call once
void cmd_init() {
  Serial.begin(9600);
  Serial.setTimeout(2);
  Serial.println("cmd_init");
  _cmd_a = 0;
  _cmd_i = 0;
}

// call every loop
void cmd_parse() {
  if (cmd_state == CMD_VALID) return;

  if (cmd_state == CMD_INVALID) {
    // keep parsing
    if (Serial.available()) {
      char tmp[CMD_ARG_N * CMD_ARG_MAX + 16];
      size_t rd = Serial.readBytesUntil(';', tmp, CMD_ARG_N * CMD_ARG_MAX + 16);
      tmp[rd] = '\0';
      
      int i, j = 0;
      int c_arg = 0;
      
      // parse argv[0]
      for (i = 0; i < rd && tmp[i] != ':'; ++i) {
        cmd_argv[c_arg][j] = tmp[i];
        j++;
      }
      // skip through ':'
      i++;

      // start parsing argv[1]
      c_arg = 1;
      j = 0;
      
      while (i < rd && c_arg < CMD_ARG_N) {

        if (tmp[i] == ',') {
          cmd_argv[c_arg][j] = '\0';

          // advance argument pointer
          c_arg++;
          j = 0;
        } else {
          // just copy over
          cmd_argv[c_arg][j] = tmp[i];
        }
        
        i++;
        j++;
      }

      cmd_argv[c_arg][j] = '\0';
      cmd_argc = c_arg + 1;

      cmd_state = CMD_VALID;

      // debug
      //for (i = 0; i < cmd_argc; ++i) {
      //  Serial.println(cmd_argv[i]);
      //}
    }
  }
}

void cmd_response(const char * val) {
  Serial.println(val);
}

// relinquish and 'use' the parsed command
void cmd_used() {
  cmd_state = CMD_INVALID;
}





#include <Servo.h>
Servo myservo;

void setup() {
  cmd_init();
  myservo.attach(9);
}

void loop() {
  // keep the parser going
  cmd_parse();

  if (cmd_state == CMD_VALID) {
    // we have a command to act on
    
    if (strcmp(cmd_argv[0], "servo") == 0) {

      // 'servo' command
      int val = atoi(cmd_argv[1]);
      myservo.write(val);

      cmd_response("success");
    } else if (strcmp(cmd_argv[0], "") == 0) { 
      //
      cmd_response("success");
    } else {
      // unknown command, just pass on and sync to
      cmd_response(cmd_argv[0]);
    }

    // dispose of command
    cmd_used();
  }

}
