// modified servo example by Ryan Davis 10/1/19 

#include <Servo.h>
#define MAX 130
#define MIN 35
Servo servo1;  // create servo object to control a servo
Servo servo2;  // create servo object to control a servo

void setup() {
  
}

void attachServos();
void detachServos();
void loop() {
    attachServos();
    servo1.write(MAX);         
    servo2.write(MAX-10);         
    delay(300);                       // wait for servo to reach
    detachServos();
    delay(2000);                      // waits 2000ms bfore starting next motion
    attachServos();
    servo1.write(MIN);              
    servo2.write(MIN+10);
    delay(300);                       // wait for servo to reach
    detachServos();
    delay(2000);                      // waits 2000ms bfore starting next motion
}

void attachServos() {
    servo1.attach(5);
    servo2.attach(6);
}

void detachServos() {
    servo1.detach();
    servo2.detach();
}

