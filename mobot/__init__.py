
import threading
import logging
import time

# py serial
import serial

# set to debug
#logging.basicConfig(level=logging.DEBUG)
logging.basicConfig(level=logging.INFO)


# local libs
from . import horserider

def fail(msg):
    print ("FATAL ERROR: " + str(msg))
    exit(1)

class Command:

    def __init__(self):
        self.robot = None

        # set state variables
        self.isActive = False

    # internal methods begin with _
    def _start(self):
        self.isActive = True
        self.start()

    def _stop(self):
        self.isActive = False
        self.stop()

    def _run(self):
        self.run()


    # The writer of each Command needs to override these
    def start(self):
        pass

    def stop(self):
        pass

    def run(self):
        pass


class IntervalTask:

    def __init__(self, func, interval=1.0):

        self.func = func
        self.interval = interval

        self.stop()

        self.thread = threading.Thread(None, self.run)
        self.thread.start()

    def start(self):
        self.isActive = True

    def stop(self):
        self.isActive = False

    def run(self):
        while True:
            while not self.isActive:
                time.sleep(0.1)

            try:
                logging.debug("IntervalTask")
                self.func()
            except Exception as e:
                print ("ERROR RUNING TASK")

            time.sleep(self.interval)



class Robot:

    def __init__(self, name, commands=[]):
        self.name = name
        self.commands = commands

        self.linkWithCommands()

        # key = command
        self._schedules = { }

    
    def get_task(self, func):
        return self._schedules[func]

    def schedule(self, func, interval):
        if self._schedules.get(func, None) is None:
            self._schedules[func] = IntervalTask(func, interval)
            self._schedules[func].start()
        else:
            self._schedules[func].interval = interval

        return self._schedules[func]

    def addCommand(self, command):
        self.commands += [command]
        
        self.linkWithCommands()

    def linkWithCommands(self):
        # method to link commands
        for i, command in enumerate(self.commands):
            try:
                if command.robot is not None and command.robot is not self:
                    raise Exception("Command already has a robot assigned! (command.robot=%s)" % (str(command.robot)))
                else:
                    command.robot = self
            except Exception as e:
                print ("Error setting robot to command %d (of type %s): %s" % (i, type(command).__name__, str(e)))


    # runner functions for the commands
    def start(self):
        logging.debug("Starting robot '%s'..." % (self.name, ))
        for i, command in enumerate(self.commands):
            try:
                command._start()
            except Exception as e:
                print ("Error starting command %d (of type %s): %s" % (i, type(command).__name__, str(e)))
    
        self.schedule(self.run, 0.100)

    def stop(self):
        logging.debug("Stopping robot '%s'..." % (self.name, ))
        for i, command in enumerate(self.commands):
            try:
                command._stop()
            except Exception as e:
                print ("Error stopping command %d (of type %s): %s" % (i, type(command).__name__, str(e)))

    def run(self):
        logging.debug("Running robot '%s'..." % (self.name, ))
        for i, command in enumerate(self.commands):
            try:
                command.run()
            except Exception as e:
                print ("Error running command %d (of type %s): %s" % (i, type(command).__name__, str(e)))



