
import logging

import serial

import time

# global cache of resources so a single TTY can be a shared resources

class HorseRider:

    __sources = {  }
    
    # serial_src = '/dev/ttyACM0' is the default
    def __init__(self, serial_src):
        self.serial_src = serial_src

        if self.serial_src not in HorseRider.__sources.keys():
            HorseRider.__sources[self.serial_src] = serial.Serial(self.serial_src, 9600)

            while len(HorseRider.__sources[self.serial_src] .readline()) < 2:
                time.sleep(0.1)


    def get_source(self):
        #global __sources
        return HorseRider.__sources.get(self.serial_src, None)

    def send_command(self, cmd, *args):
        command_text = str(cmd) + ":" + ",".join(list(map(str, args))) + ";"

        logging.debug("Sending HorseRider command: '" + command_text + "'")
        src = self.get_source()
        src.write(command_text.encode("ascii"))

        # read respones
        return src.readline().decode()

